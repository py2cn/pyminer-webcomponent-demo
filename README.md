# 这里是用于LigralPy等项目的npm包模板

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
### 构建依赖

首先要确保在index.ts里面定义了需要导出的内容。

```
yarn lib
npm login
npm install -g nrm
```

确认registry为官方源。
可以用nrm npm来切换过去。
```sh
> npm config get registry
https://registry.npmjs.org/
```